package hu.tabi.katalin.miniweather.util;

import android.databinding.BindingAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import hu.tabi.katalin.miniweather.R;
import hu.tabi.katalin.miniweather.viewmodel.SearchViewModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class BindingUtil {

    @BindingAdapter("setPagerAdapter")
    public static void setPagerAdapter(ViewPager viewPager, PagerAdapter pagerAdapter) {
        viewPager.setAdapter(pagerAdapter);
    }

    @BindingAdapter("setAdapter")
    public static void setAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter("setTextListener")
    public static void setTextListener(EditText editText, final SearchViewModel searchViewModel) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchViewModel.setCityName(editable.toString());
            }
        });
    }

    @BindingAdapter("setTemp")
    public static void setTemp(TextView textView, float temp) {
        textView.setText(String.format(Locale.getDefault(), "%s °C", temp));
    }

    @BindingAdapter("setDate")
    public static void setDate(TextView textView, long date) {
        textView.setText(SimpleDateFormat.getInstance().format(new Date(date * 1000)));
    }

    @BindingAdapter("setIcon")
    public static void setIcon(ImageView imageView, String icon) {
        String url = "http://openweathermap.org/img/w/" + icon + ".png";
        Glide.with(imageView).load(url).into(imageView);

    }

    @BindingAdapter("setSelected")
    public static void setSelected(ImageView imageView, boolean selected) {
        if (selected) {
            imageView.setImageResource(R.drawable.ic_favourite);
        } else {
            imageView.setImageResource(R.drawable.ic_favourite_border);
        }
    }


}
