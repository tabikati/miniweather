package hu.tabi.katalin.miniweather.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import hu.tabi.katalin.miniweather.view.BaseFragment;
import hu.tabi.katalin.miniweather.view.FavouritesFragment;
import hu.tabi.katalin.miniweather.view.SearchFragment;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class MainPagerAdapter extends FragmentPagerAdapter {

    private final List<BaseFragment> fragmentList;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentList = new ArrayList<>();
        fragmentList.add(new SearchFragment());
        fragmentList.add(new FavouritesFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentList.get(position).getTitle();
    }
}
