package hu.tabi.katalin.miniweather.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;

import hu.tabi.katalin.miniweather.adapter.MainPagerAdapter;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class MainViewModel extends BaseObservable {


    @Bindable
    private MainPagerAdapter adapter;


    public MainViewModel(AppCompatActivity appCompatActivity) {
        adapter = new MainPagerAdapter(appCompatActivity.getSupportFragmentManager());
    }

    public MainPagerAdapter getAdapter(){
        return adapter;
    }


}
