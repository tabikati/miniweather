package hu.tabi.katalin.miniweather.view;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hu.tabi.katalin.miniweather.R;
import hu.tabi.katalin.miniweather.contract.CityContract;
import hu.tabi.katalin.miniweather.contract.SearchContract;
import hu.tabi.katalin.miniweather.databinding.FragmentSearchBinding;
import hu.tabi.katalin.miniweather.model.CityModel;
import hu.tabi.katalin.miniweather.viewmodel.SearchViewModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class SearchFragment extends BaseFragment implements SearchContract.View{

    private ProgressDialog progressDialog;
    private AlertDialog errorDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        FragmentSearchBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);
        SearchViewModel searchViewModel = new SearchViewModel(this, this);
        binding.setViewModel(searchViewModel);
        return binding.getRoot();
    }

    @Override
    public String getTitle() {
        return "Search";
    }

    @Override
    public void showLoading(boolean loading) {
        if (loading) {
            showProgressDialog();
        } else {
            hideProgressDialog();
        }
    }

    @Override
    public void showError(String errorMessage) {
        if (errorDialog == null) {
            errorDialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Error")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            errorDialog.dismiss();
                        }
                    })
                    .create();
        }
        errorDialog.setMessage(errorMessage);
        if (getActivity() != null && !getActivity().isFinishing() && !errorDialog.isShowing()) {
            errorDialog.show();
        }
    }

    private void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Searching...");
        }
        if (getActivity() != null && !getActivity().isFinishing() && !progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (getActivity() != null && !getActivity().isFinishing() && progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
