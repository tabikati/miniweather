package hu.tabi.katalin.miniweather.view;

import android.support.v4.app.Fragment;

import hu.tabi.katalin.miniweather.contract.CityContract;
import hu.tabi.katalin.miniweather.model.CityModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public abstract class BaseFragment extends Fragment implements  CityContract.View  {

    public abstract String getTitle();

    @Override
    public void showWeather(CityModel cityModel) {
        WeatherDialogFragment.getInstance(cityModel).show(getActivity().getSupportFragmentManager(), "weather");
    }
}
