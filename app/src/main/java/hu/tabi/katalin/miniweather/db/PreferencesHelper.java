package hu.tabi.katalin.miniweather.db;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import hu.tabi.katalin.miniweather.model.CityModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class PreferencesHelper {

    private static final String PREF = "weather";
    private static final String KEY_FAVOURITES = "favourites";

    private SharedPreferences preferences;
    private static PreferencesHelper instance;

    private PreferencesHelper(Context context) {
        preferences = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
    }

    public static synchronized void init(Context context) {
        if (instance == null) {
            instance = new PreferencesHelper(context);
        }
    }

    public static void saveCity(CityModel cityModel) {
        List<CityModel> cityList = getCityList();
        if (cityList == null) {
            cityList = new ArrayList<>();
        }
        if (!cityList.contains(cityModel)) {
            cityList.add(cityModel);
            saveCityList(cityList);
        }
    }

    public static void removeCity(CityModel cityModel) {
        List<CityModel> cityList = getCityList();
        if (cityList != null && cityList.contains(cityModel)) {
            cityList.remove(cityModel);
            saveCityList(cityList);
        }
    }

    public static List<CityModel> getCityList() {
        String json = instance.preferences.getString(KEY_FAVOURITES, null);
        if (json == null || json.isEmpty()) {
            return new ArrayList<>();
        }
        return new Gson().fromJson(json, new TypeToken<List<CityModel>>() {
        }.getType());
    }

    private static void saveCityList(List<CityModel> cityList) {
        instance.preferences.edit().putString(KEY_FAVOURITES, new Gson().toJson(cityList, new TypeToken<List<CityModel>>() {
        }.getType())).apply();
    }

    public static boolean isFavourite(CityModel cityModel) {
        List<CityModel> cityList = getCityList();
        return cityList != null && cityList.contains(cityModel);
    }

}
