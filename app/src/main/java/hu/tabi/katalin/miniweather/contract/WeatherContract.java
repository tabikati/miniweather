package hu.tabi.katalin.miniweather.contract;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class WeatherContract {

    public interface View {
        void showLoading(boolean loading);

        void showError(String errorMessage);
    }

    public interface ViewModel{
        void onFavouriteClicked();
    }
}
