package hu.tabi.katalin.miniweather.api.resonse_model;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class Sys {

    private long sunrise;
    private long sunset;

    public long getSunrise() {
        return sunrise;
    }

    public long getSunset() {
        return sunset;
    }
}
