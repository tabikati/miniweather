package hu.tabi.katalin.miniweather.api;

import java.util.List;

import hu.tabi.katalin.miniweather.api.resonse_model.CityListResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.ForecastResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.WeatherResponseModel;
import hu.tabi.katalin.miniweather.model.CityModel;
import hu.tabi.katalin.miniweather.model.WeatherModel;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class WeatherApiUtil {


    public static void searchCityByName(String cityName, final ResponseCallback<List<CityModel>> responseCallback) {
        WeatherApi.getInstance().findCityByCityName(cityName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<CityListResponseModel>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull CityListResponseModel cityListResponseModel) {
                        responseCallback.onSuccess(ResponseConverterUtil.convertCityList(cityListResponseModel));

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        responseCallback.onError(e.getMessage());
                    }
                });
    }

    public static void downloadWeather(int cityId, final ResponseCallback<WeatherModel> responseCallback) {
        WeatherApi.getInstance().getWeather(cityId).zipWith(WeatherApi.getInstance().getForecast(cityId), new BiFunction<WeatherResponseModel, ForecastResponseModel, WeatherModel>() {
            @Override
            public WeatherModel apply(WeatherResponseModel weatherResponseModel, ForecastResponseModel forecastResponseModel) throws Exception {
                return ResponseConverterUtil.convertWeatherModel(weatherResponseModel, forecastResponseModel);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<WeatherModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(WeatherModel weatherModel) {
                        responseCallback.onSuccess(weatherModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        responseCallback.onError(e.getMessage());
                    }
                });
    }
}
