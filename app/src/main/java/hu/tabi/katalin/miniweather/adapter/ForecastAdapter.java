package hu.tabi.katalin.miniweather.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import hu.tabi.katalin.miniweather.BR;
import hu.tabi.katalin.miniweather.R;
import hu.tabi.katalin.miniweather.viewmodel.DayItemViewModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    private List<DayItemViewModel> dayItemViewModelList;

    public void setForecastData(List<DayItemViewModel> dayItemViewModelList) {
        this.dayItemViewModelList = dayItemViewModelList;
        notifyDataSetChanged();
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_day, parent, false);
        return new ForecastViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(ForecastViewHolder holder, int position) {
        holder.binding.setVariable(BR.viewModel, dayItemViewModelList.get(position));
    }

    @Override
    public int getItemCount() {
        if (dayItemViewModelList == null) {
            return 0;
        } else {
            return dayItemViewModelList.size();
        }
    }

    class ForecastViewHolder extends RecyclerView.ViewHolder {

        ViewDataBinding binding;

        ForecastViewHolder(ViewDataBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            binding = viewDataBinding;
        }
    }
}
