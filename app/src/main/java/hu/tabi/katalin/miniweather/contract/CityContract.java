package hu.tabi.katalin.miniweather.contract;

import hu.tabi.katalin.miniweather.model.CityModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class CityContract {

    public interface View{
        void showWeather(CityModel cityModel);
    }

    public interface ViewModel {
        void onCitySelected();
    }
}
