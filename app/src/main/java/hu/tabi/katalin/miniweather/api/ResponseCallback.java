package hu.tabi.katalin.miniweather.api;

/**
 * Created by Kati on 2017. 10. 28..
 */

public interface ResponseCallback<T> {

    void onSuccess(T object);
    void onError(String errorMessage);
}
