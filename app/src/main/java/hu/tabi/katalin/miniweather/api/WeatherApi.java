package hu.tabi.katalin.miniweather.api;

import java.util.concurrent.Callable;

import hu.tabi.katalin.miniweather.api.resonse_model.CityListResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.ForecastResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.WeatherResponseModel;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kati on 2017. 10. 28..
 */

class WeatherApi {

    private static final String API_KEY = "0cb5936c0e4f8efa9006e64cba4f0cd9";

    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    private static final String URL_FIND = "find";
    private static final String URL_WEATHER = "weather";
    private static final String URL_FORECAST = "forecast/daily";


    private static final String PARAMETER_LIKE = "like";
    private static final String PARAMETER_METRIC = "metric";

    private static WeatherApi instance;
    private final WeatherService service;

    private WeatherApi() {
        this.service = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(WeatherService.class);
    }


    static synchronized WeatherApi getInstance() {
        if (instance == null)
            instance = new WeatherApi();
        return instance;
    }


    Single<CityListResponseModel> findCityByCityName(final String cityName) {
        return Single.fromCallable(new Callable<CityListResponseModel>() {
            @Override
            public CityListResponseModel call() throws Exception {
                Response<CityListResponseModel> response = service.findCityByCityName(URL_FIND, API_KEY, cityName, PARAMETER_LIKE).execute();
                return response.body();
            }
        });
    }

    Single<WeatherResponseModel> getWeather(final int cityId) {
        return Single.fromCallable(new Callable<WeatherResponseModel>() {
            @Override
            public WeatherResponseModel call() throws Exception {
                Response<WeatherResponseModel> response = service.getWeather(URL_WEATHER, API_KEY, cityId, PARAMETER_METRIC).execute();
                return response.body();
            }
        });
    }

    Single<ForecastResponseModel> getForecast(final int cityId) {
        return Single.fromCallable(new Callable<ForecastResponseModel>() {
            @Override
            public ForecastResponseModel call() throws Exception {
                Response<ForecastResponseModel> response = service.getForecast(URL_FORECAST, API_KEY, cityId, 16, PARAMETER_METRIC).execute();
                return response.body();
            }
        });
    }
}
