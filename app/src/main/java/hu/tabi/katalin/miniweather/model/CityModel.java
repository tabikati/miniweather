package hu.tabi.katalin.miniweather.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class CityModel implements Parcelable {

    private String name;
    private int id;
    private boolean favourite;

    private CityModel(Builder builder) {
        setName(builder.name);
        setId(builder.id);
    }

    protected CityModel(Parcel in) {
        name = in.readString();
        id = in.readInt();
    }

    public static final Creator<CityModel> CREATOR = new Creator<CityModel>() {
        @Override
        public CityModel createFromParcel(Parcel in) {
            return new CityModel(in);
        }

        @Override
        public CityModel[] newArray(int size) {
            return new CityModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeInt(id);
    }

    public static final class Builder {
        private String name;
        private int id;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public CityModel build() {
            return new CityModel(this);
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CityModel && ((CityModel) obj).getId() == id || super.equals(obj);
    }
}
