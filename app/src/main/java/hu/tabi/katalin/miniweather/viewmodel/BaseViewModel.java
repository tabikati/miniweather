package hu.tabi.katalin.miniweather.viewmodel;

import android.databinding.BaseObservable;

import java.util.ArrayList;
import java.util.List;

import hu.tabi.katalin.miniweather.adapter.CityAdapter;
import hu.tabi.katalin.miniweather.contract.CityContract;
import hu.tabi.katalin.miniweather.model.CityModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class BaseViewModel extends BaseObservable {

    protected CityAdapter adapter;
    private CityContract.View parentView;

    BaseViewModel(CityContract.View parentView) {
        this.parentView = parentView;
        adapter = new CityAdapter();
    }

    public CityAdapter getAdapter() {
        return adapter;
    }

    void fillAdapter(List<CityModel> cityList) {
        List<CityItemViewModel> cityItemViewModelList = new ArrayList<>();
        for (CityModel cityModel : cityList) {
            cityItemViewModelList.add(new CityItemViewModel(parentView, cityModel));
        }
        adapter.setCityModelList(cityItemViewModelList);
    }
}
