package hu.tabi.katalin.miniweather.util;

import hu.tabi.katalin.miniweather.model.CityModel;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class RxBus {

    private static RxBus instance;

    private PublishSubject<CityModel> subject = PublishSubject.create();

    public static RxBus getInstance() {
        if (instance == null) {
            instance = new RxBus();
        }
        return instance;
    }

    public void notifyFavourites(CityModel object) {
        subject.onNext(object);
    }

    public Observable<CityModel> getFavouriteCity() {
        return subject;
    }
}
