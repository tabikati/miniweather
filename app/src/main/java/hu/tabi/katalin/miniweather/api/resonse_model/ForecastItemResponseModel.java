package hu.tabi.katalin.miniweather.api.resonse_model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class ForecastItemResponseModel {

    @SerializedName("dt")
    private long date;

    @SerializedName("temp")
    private Temp temp;

    @SerializedName("weather")
    private List<Weather> weatherList;

    public Temp getTemp() {
        return temp;
    }

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public long getDate() {
        return date;
    }
}
