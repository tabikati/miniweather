package hu.tabi.katalin.miniweather.api.resonse_model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class CityItemResponseModel {

    @SerializedName("id")
    private int cityId;

    @SerializedName("name")
    private String cityName;

    public int getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

}
