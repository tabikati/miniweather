package hu.tabi.katalin.miniweather.api.resonse_model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class CityListResponseModel {

    @SerializedName("list")
    private List<CityItemResponseModel> cityList;

    public List<CityItemResponseModel> getCityList() {
        return cityList;
    }

}
