package hu.tabi.katalin.miniweather.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hu.tabi.katalin.miniweather.R;
import hu.tabi.katalin.miniweather.contract.WeatherContract;
import hu.tabi.katalin.miniweather.databinding.DialogWeatherBinding;
import hu.tabi.katalin.miniweather.model.CityModel;
import hu.tabi.katalin.miniweather.viewmodel.WeatherViewModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class WeatherDialogFragment extends DialogFragment implements WeatherContract.View {

    private static final String KEY_CITY = "city";

    private CityModel cityModel;

    public static WeatherDialogFragment getInstance(CityModel cityModel) {
        WeatherDialogFragment dialog = new WeatherDialogFragment();
        dialog.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme);
        Bundle args = new Bundle();
        args.putParcelable(KEY_CITY, cityModel);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cityModel = getArguments().getParcelable(KEY_CITY);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogWeatherBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_weather, container, false);
        WeatherViewModel viewModel = new WeatherViewModel(this, cityModel);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void showLoading(boolean loading) {
        //TODO
    }

    @Override
    public void showError(String errorMessage) {
        //TODO
    }
}
