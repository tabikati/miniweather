package hu.tabi.katalin.miniweather.api;

import hu.tabi.katalin.miniweather.api.resonse_model.CityListResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.ForecastResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.WeatherResponseModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Kati on 2017. 10. 28..
 */

public interface WeatherService {

    String KEY_APP_ID = "appid";
    String KEY_CITY_NAME = "q";
    String KEY_CITY_ID = "id";
    String KEY_TYPE = "type";
    String KEY_UNITS = "units";
    String KEY_COUNT = "cnt";


    @GET
    Call<CityListResponseModel> findCityByCityName(@Url String url, @Query(KEY_APP_ID) String appId, @Query(KEY_CITY_NAME) String cityName, @Query(KEY_TYPE) String type);

    @GET
    Call<WeatherResponseModel> getWeather(@Url String url, @Query(KEY_APP_ID) String appId, @Query(KEY_CITY_ID) int cityId, @Query(KEY_UNITS) String units);

    @GET
    Call<ForecastResponseModel> getForecast(@Url String url, @Query(KEY_APP_ID) String appId, @Query(KEY_CITY_ID) int cityId, @Query(KEY_COUNT) int count, @Query(KEY_UNITS) String units);
}
