package hu.tabi.katalin.miniweather.api.resonse_model;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class Weather {

    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
