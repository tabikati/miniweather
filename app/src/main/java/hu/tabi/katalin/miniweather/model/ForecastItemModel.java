package hu.tabi.katalin.miniweather.model;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class ForecastItemModel {

    private long date;
    private float tempMorn;
    private float tempNight;
    private String icon;

    private ForecastItemModel(Builder builder) {
        setDate(builder.date);
        setTempMorn(builder.tempMorn);
        setTempNight(builder.tempNight);
        setIcon(builder.icon);
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public float getTempMorn() {
        return tempMorn;
    }

    public void setTempMorn(float tempMorn) {
        this.tempMorn = tempMorn;
    }

    public float getTempNight() {
        return tempNight;
    }

    public void setTempNight(float tempNight) {
        this.tempNight = tempNight;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public static final class Builder {
        private long date;
        private float tempMorn;
        private float tempNight;
        private String icon;

        public Builder() {
        }

        public Builder date(long val) {
            date = val;
            return this;
        }

        public Builder tempMorn(float val) {
            tempMorn = val;
            return this;
        }

        public Builder tempNight(float val) {
            tempNight = val;
            return this;
        }

        public Builder icon(String val) {
            icon = val;
            return this;
        }

        public ForecastItemModel build() {
            return new ForecastItemModel(this);
        }
    }
}
