package hu.tabi.katalin.miniweather.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.util.ArrayList;
import java.util.List;

import hu.tabi.katalin.miniweather.BR;
import hu.tabi.katalin.miniweather.adapter.ForecastAdapter;
import hu.tabi.katalin.miniweather.api.ResponseCallback;
import hu.tabi.katalin.miniweather.api.WeatherApiUtil;
import hu.tabi.katalin.miniweather.contract.WeatherContract;
import hu.tabi.katalin.miniweather.db.PreferencesHelper;
import hu.tabi.katalin.miniweather.model.CityModel;
import hu.tabi.katalin.miniweather.model.ForecastItemModel;
import hu.tabi.katalin.miniweather.model.WeatherModel;
import hu.tabi.katalin.miniweather.util.RxBus;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class WeatherViewModel extends BaseObservable implements WeatherContract.ViewModel {

    @Bindable
    private WeatherModel weatherModel;
    @Bindable
    private CityModel cityModel;
    @Bindable
    private ForecastAdapter adapter;

    private WeatherContract.View view;

    @Bindable
    private boolean favourite;

    public WeatherViewModel(WeatherContract.View view, CityModel cityModel) {
        this.view = view;
        this.cityModel = cityModel;
        adapter = new ForecastAdapter();
        downloadWeather();
        favourite = PreferencesHelper.isFavourite(cityModel);
    }

    public WeatherModel getWeatherModel() {
        return weatherModel;
    }

    public CityModel getCityModel() {
        return cityModel;
    }

    public ForecastAdapter getAdapter() {
        return adapter;
    }

    public void setWeatherModel(WeatherModel weatherModel) {
        this.weatherModel = weatherModel;
        notifyPropertyChanged(BR.weatherModel);
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
        notifyPropertyChanged(BR.favourite);
    }

    private void downloadWeather() {
        view.showLoading(true);
        WeatherApiUtil.downloadWeather(cityModel.getId(), new ResponseCallback<WeatherModel>() {
            @Override
            public void onSuccess(WeatherModel object) {
                view.showLoading(false);
                setWeatherModel(object);
                List<DayItemViewModel> days = new ArrayList<>();
                for (ForecastItemModel forecastItemModel : object.getForecast()) {
                    days.add(new DayItemViewModel(forecastItemModel));
                }
                adapter.setForecastData(days);
            }

            @Override
            public void onError(String errorMessage) {
                view.showLoading(false);
                view.showError(errorMessage);
            }
        });
    }

    @Override
    public void onFavouriteClicked() {
        setFavourite(!favourite);
        cityModel.setFavourite(favourite);
        if (favourite) {
            PreferencesHelper.saveCity(cityModel);
        } else {
            PreferencesHelper.removeCity(cityModel);
        }
        RxBus.getInstance().notifyFavourites(cityModel);
    }
}
