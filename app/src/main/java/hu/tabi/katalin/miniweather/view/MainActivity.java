package hu.tabi.katalin.miniweather.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import hu.tabi.katalin.miniweather.R;
import hu.tabi.katalin.miniweather.databinding.ActivityMainBinding;
import hu.tabi.katalin.miniweather.db.PreferencesHelper;
import hu.tabi.katalin.miniweather.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferencesHelper.init(this);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(new MainViewModel(this));
    }
}
