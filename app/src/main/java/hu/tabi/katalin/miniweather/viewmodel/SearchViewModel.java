package hu.tabi.katalin.miniweather.viewmodel;

import java.io.Serializable;
import java.util.List;

import hu.tabi.katalin.miniweather.api.ResponseCallback;
import hu.tabi.katalin.miniweather.api.WeatherApiUtil;
import hu.tabi.katalin.miniweather.contract.CityContract;
import hu.tabi.katalin.miniweather.contract.SearchContract;
import hu.tabi.katalin.miniweather.model.CityModel;
import hu.tabi.katalin.miniweather.util.RxBus;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class SearchViewModel extends BaseViewModel implements SearchContract.ViewModel {

    private SearchContract.View view;

    private String cityName;

    public SearchViewModel(SearchContract.View view, CityContract.View parentView) {
        super(parentView);
        this.view = view;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public void onSearchClicked() {
        adapter.clear();
        searchCityWeather();
    }

    private void searchCityWeather() {
        view.showLoading(true);
        WeatherApiUtil.searchCityByName(cityName, new ResponseCallback<List<CityModel>>() {
            @Override
            public void onSuccess(List<CityModel> cityModelList) {
                view.showLoading(false);
                fillAdapter(cityModelList);
            }

            @Override
            public void onError(String errorMessage) {
                view.showLoading(false);
                view.showError(errorMessage);
            }
        });
    }
}
