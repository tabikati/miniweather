package hu.tabi.katalin.miniweather.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import hu.tabi.katalin.miniweather.contract.CityContract;
import hu.tabi.katalin.miniweather.model.CityModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class CityItemViewModel extends BaseObservable implements CityContract.ViewModel {

    private CityContract.View parentView;

    @Bindable
    private CityModel cityModel;

    CityItemViewModel(CityContract.View parentView, CityModel cityModel) {
        this.parentView = parentView;
        this.cityModel = cityModel;
    }

    public CityModel getCityModel() {
        return cityModel;
    }

    @Override
    public void onCitySelected() {
        parentView.showWeather(cityModel);
    }


}
