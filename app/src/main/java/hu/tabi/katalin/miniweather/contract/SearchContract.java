package hu.tabi.katalin.miniweather.contract;

import hu.tabi.katalin.miniweather.model.CityModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class SearchContract {

    public interface View{
        void showLoading(boolean loading);
        void showError(String errorMessage);
    }


    public interface ViewModel {
        void onSearchClicked();
    }
}
