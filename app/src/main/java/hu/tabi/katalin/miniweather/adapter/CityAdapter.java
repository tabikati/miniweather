package hu.tabi.katalin.miniweather.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import hu.tabi.katalin.miniweather.BR;
import hu.tabi.katalin.miniweather.R;
import hu.tabi.katalin.miniweather.model.CityModel;
import hu.tabi.katalin.miniweather.viewmodel.CityItemViewModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    private List<CityItemViewModel> cityItemViewModelList;

    public void setCityModelList(List<CityItemViewModel> cityItemViewModelList) {
        this.cityItemViewModelList = cityItemViewModelList;
        notifyDataSetChanged();
    }

    public void clear() {
        if (cityItemViewModelList != null) {
            cityItemViewModelList.clear();
            notifyDataSetChanged();
        }
    }

    public void addCity(CityItemViewModel cityItemViewModel) {
        if (cityItemViewModelList == null) {
            cityItemViewModelList = new ArrayList<>();
        }
        cityItemViewModelList.add(cityItemViewModel);
        notifyItemInserted(cityItemViewModelList.size());
    }

    public void removeCity(CityModel cityModel) {
        if (cityItemViewModelList != null) {
            for (int i = 0; i < cityItemViewModelList.size(); i++) {
                if (cityItemViewModelList.get(i).getCityModel().equals(cityModel)) {
                    cityItemViewModelList.remove(i);
                    notifyItemRemoved(i);
                    break;
                }
            }
        }
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_city, parent, false);
        return new CityViewHolder(viewDataBinding);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        holder.binding.setVariable(BR.viewModel, cityItemViewModelList.get(position));
    }

    @Override
    public int getItemCount() {
        if (cityItemViewModelList == null) {
            return 0;
        } else {
            return cityItemViewModelList.size();
        }
    }

    class CityViewHolder extends RecyclerView.ViewHolder {

        ViewDataBinding binding;

        CityViewHolder(ViewDataBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            binding = viewDataBinding;
        }
    }
}
