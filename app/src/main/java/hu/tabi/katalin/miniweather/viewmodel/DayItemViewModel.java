package hu.tabi.katalin.miniweather.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import hu.tabi.katalin.miniweather.model.ForecastItemModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class DayItemViewModel extends BaseObservable {

    @Bindable
    private ForecastItemModel forecastItemModel;

    DayItemViewModel(ForecastItemModel forecastItemModel) {
        this.forecastItemModel = forecastItemModel;
    }

    public ForecastItemModel getForecastItemModel() {
        return forecastItemModel;
    }
}
