package hu.tabi.katalin.miniweather.api.resonse_model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class WeatherResponseModel {


    @SerializedName("weather")
    private List<Weather> weatherList;

    @SerializedName("main")
    private Main main;

    @SerializedName("sys")
    private Sys sys;

    @SerializedName("id")
    private int cityId;

    @SerializedName("name")
    private String cityName;

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public Main getMain() {
        return main;
    }

    public Sys getSys() {
        return sys;
    }

    public int getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

}
