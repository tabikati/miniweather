package hu.tabi.katalin.miniweather.api;

import java.util.ArrayList;
import java.util.List;

import hu.tabi.katalin.miniweather.api.resonse_model.CityItemResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.CityListResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.ForecastItemResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.ForecastResponseModel;
import hu.tabi.katalin.miniweather.api.resonse_model.WeatherResponseModel;
import hu.tabi.katalin.miniweather.model.CityModel;
import hu.tabi.katalin.miniweather.model.ForecastItemModel;
import hu.tabi.katalin.miniweather.model.WeatherModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

class ResponseConverterUtil {


    static List<CityModel> convertCityList(CityListResponseModel cityListResponseModel) {
        List<CityModel> result = new ArrayList<>();
        for (CityItemResponseModel cityItem : cityListResponseModel.getCityList()) {
            CityModel cityModel = new CityModel.Builder()
                    .id(cityItem.getCityId())
                    .name(cityItem.getCityName())
                    .build();
            result.add(cityModel);
        }
        return result;
    }

    static WeatherModel convertWeatherModel(WeatherResponseModel weatherResponseModel, ForecastResponseModel forecastResponseModel) {
        return new WeatherModel.Builder()
                .sunrise(weatherResponseModel.getSys().getSunrise())
                .sunset(weatherResponseModel.getSys().getSunset())
                .icon(weatherResponseModel.getWeatherList().get(0).getIcon())
                .tempMax(weatherResponseModel.getMain().getTempMax())
                .tempMin(weatherResponseModel.getMain().getTempMin())
                .forecast(convertForecastItemModel(forecastResponseModel))
                .build();
    }

    private static List<ForecastItemModel> convertForecastItemModel(ForecastResponseModel forecastResponseModel) {
        List<ForecastItemModel> result = new ArrayList<>();
        for (ForecastItemResponseModel forecastItemResponseModel : forecastResponseModel.getForecastList()) {
            result.add(new ForecastItemModel.Builder()
                    .icon(forecastItemResponseModel.getWeatherList().get(0).getIcon())
                    .tempMorn(forecastItemResponseModel.getTemp().getMorn())
                    .tempNight(forecastItemResponseModel.getTemp().getNight())
                    .date(forecastItemResponseModel.getDate())
                    .build());
        }
        return result;
    }
}
