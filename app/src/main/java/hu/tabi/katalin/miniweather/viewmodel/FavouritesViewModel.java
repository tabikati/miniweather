package hu.tabi.katalin.miniweather.viewmodel;

import hu.tabi.katalin.miniweather.contract.CityContract;
import hu.tabi.katalin.miniweather.db.PreferencesHelper;
import hu.tabi.katalin.miniweather.model.CityModel;
import hu.tabi.katalin.miniweather.util.RxBus;
import io.reactivex.functions.Consumer;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class FavouritesViewModel extends BaseViewModel {

    public FavouritesViewModel(final CityContract.View parentView) {
        super(parentView);
        fillAdapter(PreferencesHelper.getCityList());

        RxBus.getInstance().getFavouriteCity().subscribe(new Consumer<CityModel>() {
            @Override
            public void accept(CityModel cityModel) throws Exception {
                if (cityModel.isFavourite()) {
                    adapter.addCity(new CityItemViewModel(parentView, cityModel));
                } else {
                    adapter.removeCity(cityModel);
                }
            }
        });
    }
}
