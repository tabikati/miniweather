package hu.tabi.katalin.miniweather.api.resonse_model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class Main {

    @SerializedName("temp_min")
    private float tempMin;

    @SerializedName("temp_max")
    private float tempMax;

    public float getTempMin() {
        return tempMin;
    }

    public float getTempMax() {
        return tempMax;
    }
}
