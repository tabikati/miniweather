package hu.tabi.katalin.miniweather.model;

import java.util.List;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class WeatherModel {

    private long sunset;
    private long sunrise;
    private float tempMin;
    private float tempMax;
    private String icon;
    private List<ForecastItemModel> forecast;

    private WeatherModel(Builder builder) {
        setSunset(builder.sunset);
        setSunrise(builder.sunrise);
        setTempMin(builder.tempMin);
        setTempMax(builder.tempMax);
        setIcon(builder.icon);
        setForecast(builder.forecast);
    }


    public long getSunset() {
        return sunset;
    }

    public void setSunset(long sunset) {
        this.sunset = sunset;
    }

    public long getSunrise() {
        return sunrise;
    }

    public void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    public float getTempMin() {
        return tempMin;
    }

    public void setTempMin(float tempMin) {
        this.tempMin = tempMin;
    }

    public float getTempMax() {
        return tempMax;
    }

    public void setTempMax(float tempMax) {
        this.tempMax = tempMax;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<ForecastItemModel> getForecast() {
        return forecast;
    }

    public void setForecast(List<ForecastItemModel> forecast) {
        this.forecast = forecast;
    }

    public static final class Builder {
        private long sunset;
        private long sunrise;
        private float tempMin;
        private float tempMax;
        private String icon;
        private List<ForecastItemModel> forecast;

        public Builder() {
        }

        public Builder sunset(long val) {
            sunset = val;
            return this;
        }

        public Builder sunrise(long val) {
            sunrise = val;
            return this;
        }

        public Builder tempMin(float val) {
            tempMin = val;
            return this;
        }

        public Builder tempMax(float val) {
            tempMax = val;
            return this;
        }

        public Builder icon(String val) {
            icon = val;
            return this;
        }

        public Builder forecast(List<ForecastItemModel> val) {
            forecast = val;
            return this;
        }

        public WeatherModel build() {
            return new WeatherModel(this);
        }
    }
}
