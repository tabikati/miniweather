package hu.tabi.katalin.miniweather.api.resonse_model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class ForecastResponseModel {

    @SerializedName("list")
    private List<ForecastItemResponseModel> forecastList;

    public List<ForecastItemResponseModel> getForecastList() {
        return forecastList;
    }

}
