package hu.tabi.katalin.miniweather.view;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hu.tabi.katalin.miniweather.R;
import hu.tabi.katalin.miniweather.databinding.FragmentFavouritesBinding;
import hu.tabi.katalin.miniweather.viewmodel.FavouritesViewModel;

/**
 * Created by Kati on 2017. 10. 28..
 */

public class FavouritesFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentFavouritesBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favourites, container, false);
        FavouritesViewModel favouritesViewModel = new FavouritesViewModel(this);
        binding.setViewModel(favouritesViewModel);
        return binding.getRoot();
    }

    @Override
    public String getTitle() {
        return "Favourites";
    }
}
